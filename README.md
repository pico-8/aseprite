# Aseprite
![Aseprite](https://img.shields.io/badge/Aseprite-FFFFFF?style=for-the-badge&logo=Aseprite&logoColor=#7D929E) [![Made With PICO-8](https://img.shields.io/badge/Made%20With-PICO--8-ff004d.svg?style=for-the-badge&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAlUlEQVQ4jWP8v5gBFTxOR%2BVXPfuPwp8SxIjCt%2BBG4TIxUBkMfgNZGIyi0IRmoobZxxeo0rcPocp%2FEEEJ08HvZaobyPj%2FjTpqmLAeJM2EtgMo3MHvZeqnw9X%2FXVHSUdhnP5Qw%2Fc%2B7CUVDS%2BsWFH6QpuyIT4cMT8xQBJI%2B1aHwj1%2F3RgnTVJbrKGH29egxFPWD38tUNxAAun4liexlTtMAAAAASUVORK5CYII%3D)](https://www.lexaloffle.com/pico-8.php)


Aseprite PICO-8 Palettes. This repo contains all the files to build the extension but if you just want the extension itself you only need the file:

[parlortricks-pico-8-palettes.aseprite-extension](parlortricks-pico-8-palettes.aseprite-extension)

## Installation
Below are the steps to install the extension:
1. Download the extension from the [link](parlortricks-pico-8-palettes.aseprite-extension) above.
2. Open Aseprite
3. Goto *Edit->Preferences*
4. Select *Extensions*
    1. Select *PICO-8 Palette*
    2. Click *Disabled* button
5. Click *Add Extension* button
6. Find and open the file *[parlortricks-pico-8-palettes.aseprite-extension](parlortricks-pico-8-palettes.aseprite-extension)*

If installed correctly you will now see the following options when selecting a palette:

![Palette Options](/images/palette-choices.png)

## Colour Samples
Images below show lists of the colours in the order they appear along with the HEX code
### Standard, all standard 16 colours
![Standard](/images/pico-8-standard-palette.png)
### Secret, all secret 16 colours
![Standard](/images/pico-8-secret-palette.png)
### Complete, both standard and secret colours combined
![Standard](/images/pico-8-complete-palette.png)